<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/getList', 'UserController@getLists');
Route::get('/getNews', 'UserController@getNewsLetters');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'UserController@login');
    Route::get('/login', 'UserController@login')->name('login');
    Route::post('/login', 'UserController@login_process');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', 'UserController@logout');
    Route::get('/dashboard', 'UserController@dashboard');
    Route::get('/panel', 'UserController@Panel');
    Route::get('/panel/stop/{id}', 'UserController@stopPanel');

    /*User Edit*/
    Route::get('/newsletter', 'UserController@newsletter');
    Route::post('/newsletter', 'UserController@sendMails');
    Route::get('/users', 'UserController@allUsers');
    Route::get('/emaillists', 'UserController@allEmailLists');

    Route::get('/emaillist/create', 'UserController@newEmail');
    Route::post('/emaillist/create', 'UserController@saveEmail');
    Route::get('/emaillist/edit/{id}', 'UserController@editEmail');
    Route::post('/emaillist/edit/{id}', 'UserController@saveEmail');

    Route::get('/emaillist/delete/{id}', 'UserController@emaillistDelete');
    Route::get('/newslists', 'UserController@allNewsletters');
    Route::get('/newslist/create', 'UserController@newNewsLetter');
    Route::post('/newslist/create', 'UserController@saveNewsLetter');
    Route::get('/newslist/edit/{id}', 'UserController@editNewsLetter');
    Route::post('/newslist/edit/{id}', 'UserController@saveNewsLetter');
    Route::get('/newslist/delete/{id}', 'UserController@newsLetterDelete');
    Route::get('/user/create', 'UserController@newUser');
    Route::post('/user/create', 'UserController@addUser');
    Route::get('/user/edit/{id}', 'UserController@editUser');
    Route::post('/user/edit/{id}', 'UserController@addUser');
    Route::get('/user/delete/{id}', 'UserController@userDelete');
    Route::get('/get_templates', 'UserController@getTemplates');


    Route::get('/templates', 'UserController@allTemplates');
    Route::get('/template/create', 'UserController@newTemplate');
    Route::post('/template/create', 'UserController@saveTemplate');
    Route::get('/template/edit/{id}', 'UserController@editTemplate');
    Route::post('/template/edit/{id}', 'UserController@saveTemplate');
    Route::get('/template/delete/{id}', 'UserController@templateDelete');
    Route::get('/template/view/{id}', 'UserController@seeTemplate');

});
