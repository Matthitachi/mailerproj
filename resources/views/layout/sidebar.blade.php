<aside class="app-navbar">
    <!-- begin sidebar-nav -->
    <div class="sidebar-nav scrollbar scroll_light">
        <ul class="metismenu " id="sidebarNav">
            <li class="nav-static-title">Dashboard</li>
            <li><a class="has-arrow" href="/dashboard" aria-expanded="false"><i
                            class="nav-icon ti ti-dashboard"></i><span class="nav-title">Dashboard</span></a></li>
            <li class="nav-static-title">Dashboard Information</li>

            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-vector"></i>
                    <span class="nav-title">Mail Campaigns</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href='/panel'>Control Panel</a></li>
                    {{--                    <li> <a href='/investment/create'>Create Investment</a> </li>--}}
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-ink-pen"></i>
                    <span class="nav-title">Send Emails</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href='/newsletter'>Create</a></li>
                    {{--                    <li> <a href='/investment/create'>Create Investment</a> </li>--}}
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-new-window"></i>
                    <span class="nav-title">News Letters</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href='/newslists'>News Letter Lists</a></li>
                    <li><a href='/newslist/create'>Create</a></li>
                    {{--                    <li> <a href='/investment/create'>Create Investment</a> </li>--}}
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-email"></i>
                    <span class="nav-title">Emails</span>
                </a>

                <ul aria-expanded="false">
                    <li><a href='/emaillists'>Email Lists</a></li>
                    <li><a href='/emaillist/create'>Create</a></li>
                    {{--                    <li> <a href='/investment/create'>Create Investment</a> </li>--}}
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-archive"></i>
                    <span class="nav-title">Mail Templates</span>
                </a>

                <ul aria-expanded="false">
                    <li><a href='/templates'>Template Lists</a></li>
                    <li><a href='/template/create'>Create</a></li>
                    {{--                    <li> <a href='/investment/create'>Create Investment</a> </li>--}}
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                    <i class="nav-icon ti ti-user"></i>
                    <span class="nav-title">Users</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href='/users'>All users</a></li>
                    <li><a href='/user/create'>Create user</a></li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- end sidebar-nav -->
</aside>