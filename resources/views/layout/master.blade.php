<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dashboard - @yield('title')</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content="3ioStudio"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- app favicon -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <!-- plugin stylesheets -->
    <link rel="stylesheet" type="text/css" href="/assets/css/vendors.css"/>
    <!-- app style -->
    @stack('css')
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css"/>
</head>

<body>
<!-- begin app -->
<div class="app">
    <!-- begin app-wrap -->
    <div class="app-wrap">
        <!-- begin pre-loader -->
        <div class="loader">
            <div class="h-100 d-flex justify-content-center">
                <div class="align-self-center">
                    <img src="/assets/img/loader/loader.svg" alt="loader">
                </div>
            </div>
        </div>
        <!-- end pre-loader -->
    @component('layout.header')
    @endcomponent
    <!-- begin app-header -->

        <!-- end app-header -->
        <!-- begin app-container -->
        <div class="app-container">
            <!-- begin app-nabar -->

        @component('layout.sidebar')
        @endcomponent
        <!-- end app-navbar -->
            <!-- begin app-main -->
            <div class="app-main" id="main">
                <!-- begin container-fluid -->

                <div style="margin-top: 70px">@include('flash::message')</div>
            @yield('content')
            <!-- end container-fluid -->
            </div>
            <!-- end app-main -->
        </div>
        <!-- end app-container -->
        <!-- begin footer -->
        <footer class="footer">
            <div class="row">
                <div class="col-12 col-sm-6 text-center text-sm-left">
                    <p>&copy; Copyright 2020. All rights reserved.</p>
                </div>
                <div class="col  col-sm-6 ml-sm-auto text-center text-sm-right">
                    <p>Hand-crafted with <i class="fa fa-heart text-danger mx-1"></i> by 3ioStudio</p>
                </div>
            </div>
        </footer>
        <!-- end footer -->
    </div>
    <!-- end app-wrap -->
</div>
<!-- end app -->

<!-- plugins -->
<script src="/assets/js/vendors.js"></script>
@stack('js')
<!-- custom app -->
<script src="/assets/js/app.js"></script>
</body>


</html>