@extends('layout.master')
@section('title', 'Templates Page')

@section('content')
    <div class="container-fluid">
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 m-b-30">
                <!-- begin page title -->
                <div class="d-block d-sm-flex flex-nowrap align-items-center">
                    <div class="page-title mb-2 mb-sm-0">
                        <h1>@if(@$newslist) Edit @else Create @endif Template</h1>
                    </div>
                    <div class="ml-auto d-flex align-items-center">
                        <nav>
                            <ol class="breadcrumb p-0 m-b-0">
                                <li class="breadcrumb-item">
                                    <a href="/dashboard"><i class="ti ti-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/templates"> All Template </a>
                                </li>
                                <li class="breadcrumb-item active text-primary" aria-current="page">@if(@$newslist)
                                        Edit @else Create @endif Template
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- end page title -->
            </div>
        </div>
        <!-- end row -->
        <!-- begin row -->
        <div class="row">

            <div class="col-md-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <div class="card-heading">
                            <h4 class="card-title">@if(@$template) Edit @else Create @endif Template</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="" method="post"
                              action="{{(@$template)?'/template/edit/'.$template->id:'/template/create'}}">
                            @if ($errors->any())
                                <div class="alert alert-danger margin-top-10">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Name</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="Name"
                                           required name="name"
                                           value="{{(@old('name'))?@old('name'):(@$template? @$template->name:'')}}">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Mail Type</label>
                                    <select class="form-control" name="type" onchange="mailType(this.value, false)">
                                        <option value="0" {{(@$template->type == 'generate')?'selected':''}}>Create
                                            Template
                                        </option>
                                        <option value="1" {{(@$template->type == 'html')?'selected':''}}>Add Html
                                            Source
                                        </option>
                                    </select>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                            </div>

                            <div class="form-row" id="mailTemplate">
                                {{--                                <script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.0/plugins/media/plugin.min.js"></script>--}}

                                <div class="col-md-12 mb-3">
                                    <label for="validationAddress04">Description</label>
                                    <textarea class="form-control" id="textarea" rows="5"
                                              placeholder="Address" name="body"> {{@$template->template}} </textarea>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')

    <script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.0/tinymce.min.js"></script>

    <script>

        function initTinymce() {
            console.log('init tiny');
            $(document).ready(function () {
                tinymce.init({
                    selector: '#textarea',
                    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                    imagetools_cors_hosts: ['picsum.photos'],
                    menubar: 'file edit view insert format tools table help',
                    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                    toolbar_sticky: true,
                    image_advtab: true,
                    content_css: '//www.tiny.cloud/css/codepen.min.css',
                    link_list: [
                        {title: 'My page 1', value: 'http://www.tinymce.com'},
                        {title: 'My page 2', value: 'http://www.moxiecode.com'}
                    ],
                    image_list: [
                        {title: 'My page 1', value: 'http://www.tinymce.com'},
                        {title: 'My page 2', value: 'http://www.moxiecode.com'}
                    ],
                    image_class_list: [
                        {title: 'None', value: ''},
                        {title: 'Some class', value: 'class-name'}
                    ],
                    importcss_append: true,
                    height: 400,
                    file_picker_callback: function (callback, value, meta) {
                        /* Provide file and text for the link dialog */
                        if (meta.filetype === 'file') {
                            callback('https://www.google.com/logos/google.jpg', {text: 'My text'});
                        }

                        /* Provide image and alt text for the image dialog */
                        if (meta.filetype === 'image') {
                            callback('https://www.google.com/logos/google.jpg', {alt: 'My alt text'});
                        }

                        /* Provide alternative source and posted for the media dialog */
                        if (meta.filetype === 'media') {
                            callback('movie.mp4', {
                                source2: 'alt.ogg',
                                poster: 'https://www.google.com/logos/google.jpg'
                            });
                        }
                    },
                    templates: [
                        {
                            title: 'New Table',
                            description: 'creates a new table',
                            content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
                        },
                        {
                            title: 'Starting my story',
                            description: 'A cure for writers block',
                            content: 'Once upon a time...'
                        },
                        {
                            title: 'New list with dates',
                            description: 'New List with dates',
                            content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
                        }
                    ],
                    template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
                    template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
                    height: 600,
                    image_caption: true,
                    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
                    noneditable_noneditable_class: "mceNonEditable",
                    toolbar_mode: 'sliding',
                    contextmenu: "link image imagetools table"
                });
            });

        }

        @if(@$template == null || @$template->type == 'generate' )
        initTinymce();
        @endif
    </script>

@endpush