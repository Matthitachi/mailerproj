@extends('layout.master')
@section('title', 'User Page')

@section('content')
    <div class="container-fluid">
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 m-b-30">
                <!-- begin page title -->
                <div class="d-block d-sm-flex flex-nowrap align-items-center">
                    <div class="page-title mb-2 mb-sm-0">
                        <h1>@if(@$user) Edit @else Create @endif Client</h1>
                    </div>
                    <div class="ml-auto d-flex align-items-center">
                        <nav>
                            <ol class="breadcrumb p-0 m-b-0">
                                <li class="breadcrumb-item">
                                    <a href="/dashboard"><i class="ti ti-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/users"> All Users </a>
                                </li>
                                <li class="breadcrumb-item active text-primary" aria-current="page">@if(@$user)
                                        Edit @else Create @endif Users
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- end page title -->
            </div>
        </div>
        <!-- end row -->
        <!-- begin row -->
        <div class="row">

            <div class="col-md-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <div class="card-heading">
                            <h4 class="card-title">@if(@$user) Edit @else Create @endif User</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="" method="post" action="{{(@$user)?'/user/edit/'.$user->id:'/user/create'}}">
                            @if ($errors->any())
                                <div class="alert alert-danger margin-top-10">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Name</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="Name"
                                           required name="name"
                                           value="{{(@old('name'))?@old('name'):(@$user? @$user->name:'')}}">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustomUsername">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                        </div>
                                        <input type="email" class="form-control" id="validationCustomUsername"
                                               placeholder="Email" aria-describedby="inputGroupPrepend" name="email"
                                               value="{{(@old('email'))?@old('email'):(@$user? @$user->email:'')}}"
                                               required>
                                        <div class="invalid-feedback">
                                            Please choose a username.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(!@$user)
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationphone01">Password</label>
                                        <input type="password" class="form-control" id="validationphone01"
                                               placeholder="Enter password" name="password" required>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCity02">Confirm Password</label>
                                        <input type="password" class="form-control" id="validationCity02"
                                               placeholder="Confirm password" name="cpassword">
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>

                                </div>
                            @endif

                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
@endsection