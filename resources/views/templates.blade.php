@extends('layout.master')
@section('title', 'Template Page')
@push('css')
    <link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 m-b-30">
                <!-- begin page title -->
                <div class="d-block d-sm-flex flex-nowrap align-items-center">
                    <div class="page-title mb-2 mb-sm-0">
                        <h1>Template Lists</h1>
                    </div>
                    <div class="ml-auto d-flex align-items-center">
                        <nav>
                            <ol class="breadcrumb p-0 m-b-0">
                                <li class="breadcrumb-item">
                                    <a href="/dashboard"><i class="ti ti-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    Pages
                                </li>
                                <li class="breadcrumb-item active text-primary" aria-current="page">Template Lists
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- end page title -->
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <form method="get" action="/templates">
                        <div class="card-body row">

                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="from" name="from"
                                           value="{{@request('from')}}">
                                    <label class="">From</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="to" name="to"
                                           value="{{@request('to')}}">
                                    <label class="">To</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <select class="form-control" title="Limit" name="limit">
                                        <option value=""></option>
                                        <option value="20" {{(request('limit') =='20')?'Selected':''}}>20</option>
                                        <option value="50" {{(request('limit') =='50')?'Selected':''}}>50</option>
                                        <option value="100" {{(request('limit') =='100')?'Selected':''}}>100</option>
                                        <option value="200" {{(request('limit') =='200')?'Selected':''}}>200</option>
                                    </select>
                                    <label class="">Limit</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <select class="form-control" title="Limit" name="sort">
                                        <option value=""></option>
                                        <option value="id" {{(request('sort') =='id')?'Selected':''}}>ID</option>
                                        <option value="name" {{(request('sort') =='name')?'Selected':''}}>Name</option>
                                        <option value="type" {{(request('sort') =='type')?'Selected':''}}>Type
                                        </option>
                                        <option value="created_at" {{(request('sort') =='created_at')?'Selected':''}}>
                                            Created
                                        </option>
                                    </select>
                                    <label class="mdl-textfield__label">Sort By</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <select class="form-control" title="Limit" name="order">
                                        <option value=""></option>
                                        <option value="asc" {{(request('order') =='asc')?'Selected':''}}>Ascending
                                        </option>
                                        <option value="desc" {{(request('order') =='desc')?'Selected':''}}>Descending
                                        </option>
                                    </select>
                                    <label class="mdl-textfield__label">Order By</label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-t-20">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="txtFirstName" name="search"
                                           value="{{@request('search')}}">
                                    <label class="">Search</label>
                                </div>
                            </div>

                            <div class="col-lg-12 p-t-20 text-center">
                                <button type="submit" class="btn btn-outline-primary">Filter</button>
                                <button type="reset" class="btn btn-outline-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- start-clients contant-->
        <div class="row">
            <div class="col-12">
                <div class="card card-statistics clients-contant">
                    <div class="card-header">
                        <div class="d-xxs-flex justify-content-between align-items-center">
                            <div class="card-heading">
                                <h4 class="card-title">Templates</h4>
                            </div>
                            <div class="mt-xxs-0 mt-3 btn-group">

                                {{--                                <label class="btn btn-sm btn-outline-primary">--}}
                                {{--                                    <input type="radio" name="options" id="option2">--}}
                                {{--                                    Week--}}
                                {{--                                </label>--}}
                                {{--                                <label class="btn btn-sm btn-round btn-outline-primary">--}}
                                {{--                                    <input type="radio" name="options" id="option3"> Month--}}
                                {{--                                </label>--}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-0 table-responsive">
                        <table class="table clients-contant-table mb-0">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">created</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(@$lists as $list)
                                <tr>
                                    <td>

                                        <strong class="font-weight-bold">{{$list->name}}</strong>

                                    </td>

                                    <td>{{$list->type}}</td>
                                    <td>{{$list->created_at}}</td>
                                    <td>
                                        <a href="/template/view/{{$list->id}}" title="View Template" target="_blank"
                                           class="btn btn-icon btn-outline-primary btn-round mr-2 mb-2 mb-sm-0 "><i
                                                    class="ti ti-eye"></i></a>
                                        <a href="/template/edit/{{$list->id}}"
                                           class="btn btn-icon btn-outline-primary btn-round mr-2 mb-2 mb-sm-0 "><i
                                                    class="ti ti-pencil"></i></a>
                                        <button onclick="(confirm('Are you sure you want to delete User profile?'))? location.href='/template/delete/{{$list->id}}' : ''"
                                                class="btn btn-icon btn-outline-danger btn-round"><i
                                                    class="ti ti-close"></i></button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $lists->appends(\Illuminate\Support\Facades\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end-clients contant-->
    </div>
@endsection

@push('js')
    @push('js')
        <script src="/assets/js/moment.min.js"></script>
        <script src="/assets/js/bootstrap-datepicker.js"></script>
        <script>
            $(document).ready(function () {
                $('#from').datetimepicker({
                    format: 'DD-MM-YYYY'
                });
                $('#to').datetimepicker({
                    format: 'DD-MM-YYYY'
                });
            });
        </script>
    @endpush
@endpush