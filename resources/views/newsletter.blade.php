@extends('layout.master')
@section('title', 'New News Letter Page')

@section('content')
    <div class="container-fluid">
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 m-b-30">
                <!-- begin page title -->
                <div class="d-block d-sm-flex flex-nowrap align-items-center">
                    <div class="page-title mb-2 mb-sm-0">
                        <h1>@if(@$newslist) Edit @else Create @endif NewsLetter</h1>
                    </div>
                    <div class="ml-auto d-flex align-items-center">
                        <nav>
                            <ol class="breadcrumb p-0 m-b-0">
                                <li class="breadcrumb-item">
                                    <a href="/dashboard"><i class="ti ti-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="/newslists"> All News Letters </a>
                                </li>
                                <li class="breadcrumb-item active text-primary" aria-current="page">@if(@$newslist)
                                        Edit @else Create @endif Newsletters
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- end page title -->
            </div>
        </div>
        <!-- end row -->
        <!-- begin row -->
        <div class="row">

            <div class="col-md-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <div class="card-heading">
                            <h4 class="card-title">@if(@$newslist) Edit @else Create @endif Newsletter</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="" method="post"
                              action="{{(@$newslist)?'/newslist/edit/'.$newslist->id:'/newslist/create'}}">
                            @if ($errors->any())
                                <div class="alert alert-danger margin-top-10">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Name</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="Name"
                                           required name="name"
                                           value="{{(@old('name'))?@old('name'):(@$newslist? @$newslist->name:'')}}">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
@endsection