<?php

namespace App\Http\Controllers;

use App\EmailList;
use App\Job;
use App\Jobs\GetMailListJob;
use App\Mail\EmptyTimedMail;
use App\Mail\SendTimedMails;
use App\NewsLetter;
use App\Panel;
use App\Template;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    //
    public function dashboard()
    {
        return \redirect('/panel');
    }

    public function login(Request $request)
    {
        return view('login');
    }


    public function login_process(Request $request)
    {
        $request->validate
        ([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            flash('User successfully logged in!')->success();
            return Redirect::to('/dashboard');

        } else {
            flash('Invalid email or password!')->error();
            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function addUser(Request $request, $id = null)
    {
        $request->validate
        ([
            'name' => 'required',
            'email' => 'required',
        ]);
        if ($id) {
            $user = User::find($id);
        } else {
            $request->validate
            ([
                'password' => 'required',
                'cpassword' => 'required|same:password'
            ]);
            $user = new User();
            $user->password = Hash::make($request->password);
        }

        $user->name = $request->name;
        $user->email = $request->email;

        $user->save();
        if ($id) {
            flash('User account details updated successfully')->success();
        } else {
            flash('User account created successfully')->success();
        }
        return \redirect('/users');

    }


    public function allUsers(Request $request)
    {
        $users = new User();
        $limit = 20;
        $sort = 'created_at';
        $order = 'Desc';

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $users = $users->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('email', 'like', "%$search%");
            });
        }
        if ($request->has('from') && $request->has('to') && $request->from != '' && $request->to != '') {
            $from = Carbon::parse($request->from);
            $to = Carbon::parse($request->to);
            $users = $users->whereBetween('created_at', [$from, $to]);
        }

        if ($request->has('limit') && $request->limit != '') {
            $limit = $request->limit;
        }

        if ($request->has('sort') && $request->sort != "") {
            $sort = $request->sort;
        }
        if ($request->has('order') && $request->order != "") {
            $order = $request->order;
        }
        $users = $users->orderBy($sort, $order)->paginate($limit);
        return View('users', ['users' => $users]);
    }

    public function allEmailLists(Request $request)
    {
        $newsletters = NewsLetter::all();
        $list = new EmailList();
        $limit = 20;
        $sort = 'created_at';
        $order = 'Desc';

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $list = $list->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('email', 'like', "%$search%");
            });
        }
        if ($request->has('newsletter') && $request->newsletter != '') {
            $list = $list->where('newsletter_id', $request->newsletter);
        }
        if ($request->has('from') && $request->has('to') && $request->from != '' && $request->to != '') {
            $from = Carbon::parse($request->from);
            $to = Carbon::parse($request->to);
            $list = $list->whereBetween('created_at', [$from, $to]);
        }

        if ($request->has('limit') && $request->limit != '') {
            $limit = $request->limit;
        }

        if ($request->has('sort') && $request->sort != "") {
            $sort = $request->sort;
        }
        if ($request->has('order') && $request->order != "") {
            $order = $request->order;
        }
        $list = $list->orderBy($sort, $order)->paginate($limit);
        return View('emaillists', ['lists' => $list, 'newsletters' => $newsletters]);
    }

    public function emaillistDelete(Request $request, $id)
    {
        $user = EmailList::find($id);
        $user->delete();
        flash('Email list deleted successfully')->success();
        return redirect()->back();
    }

    public function allNewsletters(Request $request)
    {
        $list = new NewsLetter();
        $limit = 20;
        $sort = 'created_at';
        $order = 'Desc';

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $list = $list->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('news_id', 'like', "%$search%");
            });
        }
        if ($request->has('from') && $request->has('to') && $request->from != '' && $request->to != '') {
            $from = Carbon::parse($request->from);
            $to = Carbon::parse($request->to);
            $list = $list->whereBetween('created_at', [$from, $to]);
        }

        if ($request->has('limit') && $request->limit != '') {
            $limit = $request->limit;
        }

        if ($request->has('sort') && $request->sort != "") {
            $sort = $request->sort;
        }
        if ($request->has('order') && $request->order != "") {
            $order = $request->order;
        }
        $list = $list->orderBy($sort, $order)->paginate($limit);
        return View('newsletters', ['lists' => $list]);
    }

    public function newsLetterDelete(Request $request, $id)
    {
        $user = NewsLetter::find($id);
        $user->delete();
        flash('News Letter deleted successfully')->success();
        return redirect()->back();
    }

    public function userDelete(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();
        flash('User deleted successfully')->success();
        return redirect()->back();
    }

    public function newUser()
    {
        return View('user');
    }

    public function editUser($id)
    {
        $user = User::find($id);
        return View('user', ['user' => $user]);
    }

    public function getNewsLetters()
    {
        $getsession = $this->getSession();
        if ($getsession['status'] == 200) {
            $session = $getsession['data'];
            $payload = [
            ];

            $method = "GET";
            $url = 'https://dtsocializeltd.com/actcamp/getNames.php?sessionID=' . $session;
            $response = self::makeRequest($url, $method, $payload);
            if ($response['status'] == 200) {
                $list_response = json_decode($response['data']);
                if ($list_response->status == "ok") {
                    unset($list_response->status);
                    $list_array = (array)$list_response;
                    foreach ($list_array as $key => $value) {
                        $news_exists = NewsLetter::where('news_id', $key)->exists();
                        if (!$news_exists) {
                            $news = new NewsLetter();
                            $news->news_id = $key;
                            $news->name = $value;
                            $news->save();
                        }
                    }
                    return ["status" => 200, "message" => "News Letters retrieved successfully!!"];
                } else {
                    return ["status" => 403, "An error occured while retrieving session", 'data' => []];
                }
            } else {
                return ["status" => 403, "An error occured while retrieving session", 'data' => []];
            }
        }
    }

    public function getSession()
    {
        $payload = [
            "login" => "gnx3",
            "password" => "96ce4739e63b90f1dbb3286ca81c2ae9"
        ];
        $url = 'https://dtsocializeltd.com/actcamp/rmauthGN.php';
        $method = "POST";
        $response = self::makeRequest($url, $method, $payload);
        if ($response['status'] == 200) {
            $session_response = json_decode($response['data']);
            if ($session_response->status == "ok") {
                return ["status" => 200, "message" => "Session retrieved successfully!!", 'data' => $session_response->sessionID];
            } else {
                return ["status" => 403, "An error occured while retrieving session", 'data' => []];
            }
        } else {
            return ["status" => 403, "An error occured while retrieving session", 'data' => []];
        }
    }

    public static function makeRequest($url, $method, $payload)
    {
        $curl = curl_init();
        $data = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $method,
        );
        if ($method == 'POST') {
            $data[CURLOPT_POSTFIELDS] = $payload;
        }
        curl_setopt_array($curl, $data);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            // there was an error contacting the Paystack API
            return ['status' => 400, 'message' => 'An error ocured', 'data' => $err];
//            die('Curl returned error: ' . $err);
        }
        return ['status' => 200, 'message' => 'Success', 'data' => $response];

    }

    public function getLists()
    {
        set_time_limit(300);
        $getsession = $this->getSession();
        if ($getsession['status'] == 200) {
            $session = $getsession['data'];
            $payload = [
                "login" => "gnx3",
                "password" => "96ce4739e63b90f1dbb3286ca81c2ae9"
            ];

            $method = "GET";
            $newsletters = NewsLetter::all();
            if ($newsletters) {
                foreach ($newsletters as $newsletter) {
                    try {

                        $url = 'https://dtsocializeltd.com/actcamp/getList.php?sessionID=' . $session . '&list=' . $newsletter->news_id;
                        $response = self::makeRequest($url, $method, $payload);
                        if ($response['status'] == 200) {
                            $list_response = json_decode($response['data']);
                            if ($list_response->status == "ok") {
                                unset($list_response->status);
                                $list_array = (array)$list_response;
                                $ar_chunk = array_chunk($list_array, 200);
                                foreach ($ar_chunk as $chunk) {
                                    $job = (new GetMailListJob($newsletter->news_id, $chunk))->onQueue('emaillist');
                                    $this->dispatch($job);
                                }

                            }
                        }
                    } catch (\Exception $e) {
                        print($e->getMessage());
                    }
                }
            }
        }
    }

    public function QueueGetList($news_id, $data)
    {
        foreach ($data as $email_list) {
            $email_exists = EmailList::where('email', $email_list->email)->exists();
            if (!$email_exists) {
                $email_object = new EmailList();
                $email_object->name = $email_list->name;
                $email_object->email = $email_list->email;
                $email_object->newsletter_id = $news_id;
                $email_object->save();
            }
        }
    }

    public function newsletter()
    {
        $newsLetters = NewsLetter::all();
        return View('mailer', ['newsletters' => $newsLetters]);
    }

    public function sendMails(Request $request)
    {
        $mail_per_hour = 150;
        $request->validate([
            'subject' => "required",
        ]);
        $type = $request->type;
        if ($type < 2) {
            $request->validate([
                'body' => "required",
            ]);
        }
        $content = $request->body;
        $subject = $request->subject;

        if ($request->has('save') && $request->save == 1) {
            $name = 'Template_' . $this->gen_unique_id(5);
            if ($type == 0) {
                $Ttype = 'generate';
            } else {
                $Ttype = 'html';
            }
            $template = new Template();
            $template->name = $name;
            $template->template = $content;
            $template->type = $Ttype;
            $template->save();
        }
        $temp = null;
        if ($request->has('template')) {
            $temp = $request->template;
        }
        $users = new EmailList();
        if ($request->has('newsletter') && $request->newsletter != "") {
            $users = $users->where('newsletter_id', $request->newsletter);
        }
        $users = $users->get();
        $mail_count = count($users);
        $lastJob = Job::orderBy('id', 'desc')->first();
        $job_start = $lastJob ? $lastJob->id + 1 : 0;
        $count = 0;
        $hours = 0;
        if ($request->has('mail_per_hour') && $request->mail_per_hour != '') {
            $mail_per_hour = $request->mail_per_hour;
        }
        $email = 'mattrusty@yahoo.com';
//        Mail::send("emails.template", ["content" => $content], function ($message) use ($email) {
//            $message->from(config("mail.from.address"), config("mail.from.name"));
//            $message->to($email);
//            $message->subject(env('APP_NAME')." Registration Successful");
//        });
//        $to = [
//                [
//                    'email' => $email,
//                    'name' => 'Mattrusty',
//                ]
//            ];
//        Mail::to($to)->later(now(), new EmptyTimedMail($content, $subject));
        foreach ($users as $user) {
            if ($count % $mail_per_hour === 0) {
                if ($count != 0) {
                    $hours++;
                }
            }
            $to = [
                [
                    'email' => $user->email,
                    'name' => $user->name,
                ]
            ];
//            Mail::to($to)->later(now()->addHours($hours), new SendTimedMails($content, $subject));
            $this->processMail($type, $subject, $content, $to, $hours, $temp);
            if ($count == 0) {
                $lastJob = Job::orderBy('id', 'desc')->first();
                $job_start = $lastJob ? $lastJob->id : 0;
            }
            $count++;

        }
        $panel = new Panel();
        $panel->newsletter_id = $request->newsletter;
        if ($request->has('save') && $request->save == 1) {
            $panel->template_id = $template->id;
        } elseif ($type == 2) {
            $panel->template_id = $temp;
        } else {
            $panel->content = $content;
        }
        $panel->job_start = $job_start;
        $lastJob = Job::orderBy('id', 'desc')->first();
        $job_end = $lastJob ? $lastJob->id : 0;
        $panel->job_end = $job_end;
        $panel->mail_count = $mail_count;
        $panel->save();
        flash('Mail added to mailing queue successfully.')->success();
        return redirect()->back();

    }

    function processMail($type, $subject, $content, $to, $hours, $id = null)
    {
        switch ($type) {
            case 0:
                Mail::to($to)->later(now()->addHours($hours), new SendTimedMails($content, $subject));
                break;
            case 1:
                Mail::to($to)->later(now()->addHours($hours), new EmptyTimedMail($content, $subject));
                break;
            case 2:
                if ($id != null) {
                    $template = Template::find($id);
                    $content = $template->template;
                    if ($template->type == 'html') {
                        Mail::to($to)->later(now()->addHours($hours), new EmptyTimedMail($content, $subject));
                    } else {
                        Mail::to($to)->later(now()->addHours($hours), new SendTimedMails($content, $subject));
                    }
                }

                break;
        }
    }

    function gen_unique_id($length)
    {
        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function newNewsLetter()
    {
        return View('newsletter');
    }

    public function editNewsLetter($id)
    {
        $newslist = NewsLetter::find($id);
        return View('newsletter', ['newslist' => $newslist]);
    }

    public function saveNewsLetter(Request $request, $id = null)
    {
        $request->validate
        ([
            'name' => 'required',
        ]);
        if ($id) {
            $newslist = NewsLetter::find($id);
        } else {
            $newslist = new NewsLetter();
            $lastnewsletter = NewsLetter::orderBy('id', 'desc')->first();
            $newslist->news_id = $lastnewsletter->id + 1;
        }
        $newslist->name = $request->name;
        $newslist->save();
        if ($id) {
            flash('Newsletter details updated successfully')->success();
        } else {
            flash('Newsletter account created successfully')->success();
        }
        return \redirect('/newslists');

    }

    public function newEmail()
    {
        $newsLetters = NewsLetter::all();
        return View('emails', ['newsletters' => $newsLetters]);
    }

    public function editEmail($id)
    {
        $email = EmailList::find($id);
        $newsLetters = NewsLetter::all();
        return View('emails', ['newsletters' => $newsLetters, 'email' => $email]);
    }

    public function saveEmail(Request $request, $id = null)
    {
        $request->validate
        ([
            'name' => 'required',
            'email' => 'required',
            'newsletter' => 'required',
        ]);
        if ($id) {
            $email = EmailList::find($id);
        } else {
            $email = new EmailList();
        }
        $email->name = $request->name;
        $email->email = $request->email;
        $email->newsletter_id = $request->newsletter;
        $email->save();
        if ($id) {
            flash('Email user details updated successfully')->success();
        } else {
            flash('Email user added created successfully')->success();
        }
        return \redirect('/emaillists');

    }

    public function getTemplates()
    {
        $temp = Template::all();
        return response()->json(['status' => 200, 'data' => $temp]);
    }

    public function allTemplates(Request $request)
    {
        $list = new Template();
        $limit = 20;
        $sort = 'created_at';
        $order = 'Desc';

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $list = $list->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('type', 'like', "%$search%");
            });
        }
        if ($request->has('from') && $request->has('to') && $request->from != '' && $request->to != '') {
            $from = Carbon::parse($request->from);
            $to = Carbon::parse($request->to);
            $list = $list->whereBetween('created_at', [$from, $to]);
        }

        if ($request->has('limit') && $request->limit != '') {
            $limit = $request->limit;
        }

        if ($request->has('sort') && $request->sort != "") {
            $sort = $request->sort;
        }
        if ($request->has('order') && $request->order != "") {
            $order = $request->order;
        }
        $list = $list->orderBy($sort, $order)->paginate($limit);
        return View('templates', ['lists' => $list]);
    }

    public function newTemplate()
    {
        return View('template');
    }

    public function editTemplate($id)
    {
        $template = Template::find($id);
        return View('template', ['template' => $template]);
    }

    public function saveTemplate(Request $request, $id = null)
    {
        $request->validate
        ([
            'name' => 'required',
            'type' => 'required',
            'body' => 'required',
        ]);
        if ($id) {
            $template = Template::find($id);
        } else {
            $template = new Template();
        }
        $template->name = $request->name;
        $type = $request->type;
        if ($type == 0) {
            $template->type = 'generate';
        } else {
            $template->type = 'html';
        }
        $template->template = $request->body;
        $template->save();
        if ($id) {
            flash('Template details updated successfully')->success();
        } else {
            flash('Template created successfully')->success();
        }
        return \redirect('/templates');
    }

    public function seeTemplate($id)
    {
        $template = Template::find($id);
        if ($template->type == 'generate') {
            return view('emails.template', ['content' => $template->template]);
        } else {
            return view('emails.empty', ['content' => $template->template]);
        }

    }

    public function templateDelete(Request $request, $id)
    {
        $temp = Template::find($id);
        $temp->delete();
        flash('Template deleted successfully')->success();
        return redirect()->back();
    }


    public function Panel(Request $request)
    {
        $list = new Panel();
        $limit = 20;
        $sort = 'created_at';
        $order = 'Desc';

        if ($request->has('search') && $request->search != '') {
            $search = $request->search;
            $list = $list->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('type', 'like', "%$search%");
            });
        }
        if ($request->has('from') && $request->has('to') && $request->from != '' && $request->to != '') {
            $from = Carbon::parse($request->from);
            $to = Carbon::parse($request->to);
            $list = $list->whereBetween('created_at', [$from, $to]);
        }

        if ($request->has('limit') && $request->limit != '') {
            $limit = $request->limit;
        }

        if ($request->has('sort') && $request->sort != "") {
            $sort = $request->sort;
        }
        if ($request->has('order') && $request->order != "") {
            $order = $request->order;
        }
        $list = $list->orderBy($sort, $order)->paginate($limit);
        $list->map(function ($obj) {
            if ($obj->mail_completed == 0) {
                $job_count = Job::whereBetween('id', [$obj->job_start, $obj->job_end])->count();
                if ($job_count == 0) {
                    $obj->mail_completed = 1;
                    $obj->save();
                }
            } else {
                $job_count = 0;
            }
//           dd($job_count);
            $obj->sent = $obj->mail_count - ($job_count + $obj->mail_failed);
            return $obj;
        });
        return View('panel', ['lists' => $list]);
    }

    public function stopPanel($id)
    {
        $panel = Panel::find($id);
        $jobs = Job::whereBetween('id', [$panel->job_start, $panel->job_end])->get();
        $count = count($jobs);
        foreach ($jobs as $job) {
            $job->delete();
        }
        $panel->mail_failed = $count;
        $panel->mail_completed = 1;
        $panel->save();
        return \redirect()->back();


    }
}
