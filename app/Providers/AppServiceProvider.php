<?php

namespace App\Providers;

use App\Panel;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Queue::failing(function (JobFailed $event) {
            // $event->connectionName
            // $event->job
            // $event->exception
            $id = $event->job->getJobId();
            $panel = Panel::where('job_start', '<=', $id)->orWhere('job_end', '>=', $id)->latest->first();
            $panel->mail_failed += 1;
            $panel->save();
        });
    }
}
