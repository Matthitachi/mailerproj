<?php

namespace App\Jobs;

use App\EmailList;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetMailListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $session;
    private $listId;
    private $data;

    public function __construct($listId, $data)
    {
        //
        $this->listId = $listId;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach ($this->data as $email_list) {
            $email_exists = EmailList::where('email', $email_list->email)->exists();
            if (!$email_exists) {
                $email_object = new EmailList();
                $email_object->name = $email_list->name;
                $email_object->email = $email_list->email;
                $email_object->newsletter_id = $this->listId;
                $email_object->save();
            }
        }

    }
}
