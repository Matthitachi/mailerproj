<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailList extends Model
{
    //
    public function Newsletter()
    {
        return $this->belongsTo('App\NewsLetter', 'newsletter_id', 'id');
    }
}
