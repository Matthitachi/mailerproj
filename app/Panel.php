<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    //

    public function Newsletter()
    {
        return $this->belongsTo('App\NewsLetter', 'newsletter_id', 'id');
    }

    public function Template()
    {
        return $this->belongsTo('App\Template', 'template_id', 'id');
    }
}
