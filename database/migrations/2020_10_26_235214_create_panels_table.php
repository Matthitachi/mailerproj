<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('newsletter_id')->unsigned()->nullable();
            $table->bigInteger('template_id')->unsigned()->nullable();
            $table->Text('content')->nullable();
            $table->integer('job_start');
            $table->integer('job_end');
            $table->integer('mail_count')->default(0);;
            $table->integer('mail_failed')->default(0);
            $table->boolean('mail_completed')->default(0);
            $table->foreign('newsletter_id')->references('id')->on('news_letters')->cascadeOnDelete();
            $table->foreign('template_id')->references('id')->on('templates')->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panels');
    }
}
