<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        Model::unguard();
        $type = [
            ['name' => 'Admin', 'email' => 'admin@test.com', 'password' => '$2y$10$nnoe1X6AmvqTENg9eM204uNoe/hDOJWjM.4m/xo4OgD8ltNlbWTMq'],

        ];
        $db = DB::table('users')->insert($type);
    }
}
